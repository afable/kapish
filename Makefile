kapish: kapish.o
	gcc -g -Wall -Werror kapish.o -o kapish

kapish.o: kapish.c
	gcc -c -g -Wall -Werror kapish.c

clean:
	rm -f *.o
	rm -f kapish
