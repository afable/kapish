Erik Afable

Last updated: January 4, 2012

@@@@@@@@@@@@@@@@@
@ kapish readme @
@@@@@@@@@@@@@@@@@

------------------------------------------------------------
program description:
------------------------------------------------------------
kapish simulates a shell by forking a child process and execing
the path of the program to run in that child process.

------------------------------------------------------------
errors handled:
------------------------------------------------------------
-continuously hitting enter or spaces
-arrow key input
-overload of input buffer
-ctrl+c (sort of) and ctrl+d (tested with top)
-invalid bin paths
-fork pID returning negative
-"cd" by itself changes current directory to home directory

------------------------------------------------------------
known bugs not handled:
------------------------------------------------------------
-tabs are not delimited by the kapish shell and are taken in as characters
-ctrl+c does not kill kapish, but you get dominoes!

------------------------------------------------------------
valgrind memory leak results (one leak not  yet handled) :
------------------------------------------------------------
==19853== 
==19853== HEAP SUMMARY:
==19853==     in use at exit: 1,024 bytes in 1 blocks
==19853==   total heap usage: 109 allocs, 108 frees, 111,616 bytes allocated
==19853== 
==19853== 1,024 bytes in 1 blocks are definitely lost in loss record 1 of 1
==19853==    at 0x4C274A8: malloc (vg_replace_malloc.c:236)
==19853==    by 0x400F35: tokenize (kapish.c:101)
==19853==    by 0x401232: main (kapish.c:206)
==19853== 
==19853== LEAK SUMMARY:
==19853==    definitely lost: 1,024 bytes in 1 blocks
==19853==    indirectly lost: 0 bytes in 0 blocks
==19853==      possibly lost: 0 bytes in 0 blocks
==19853==    still reachable: 0 bytes in 0 blocks
==19853==         suppressed: 0 bytes in 0 blocks
==19853== 
==19853== For counts of detected and suppressed errors, rerun with: -v
==19853== ERROR SUMMARY: 1 errors from 1 contexts (suppressed: 4 from 4)




