/* File: kapish.c
 * Description: kapish simulates a shell by forking a child process and
 *		exec the program to run in that child process.
 * Creator: Erik Afable
 * Created: September 26, 2011
 * Last updated: January 4, 2012
 */

/* Preprocessor directives */
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <termios.h>
#include <signal.h>

#define _XOPEN_SOURCE 500
#define MAX_INPUT 512
#define MAX_STRING_LENGTH 1024
#define MAX_COMMANDS 256
#define MAX_PATHS 256
#define PATH_LENGTH 512
#define EXECUTE_PATH -1
//#define DEBUG

typedef void (*sighandler_t)(int);
sighandler_t sigset(int sig, sighandler_t disp);

/* Description: Handles the use of ctrl+c after executing a child process.
 * Params: None.
 * Returns: None.
 */
void setup_term(void)
{
	struct termios t;
	tcgetattr(0, &t);
	t.c_lflag &= ~ECHOCTL;
	tcsetattr(0, TCSANOW, &t);
}

/* Description: Forks address space of current process to child and execs
 *		new program in child.
 * Params:
 *  	bin		the path of the executable
 *  	flags	flags associated with executable
 * Returns: pID of the child process if successful, 1 otherwise.
 */
int fork_exec(char *bin, char **flags)
{
	int childstatus;
	pid_t temp_pid;
	pid_t pID = fork();
    
	if ( !pID )           	// if pID 0, then child
		execvp(bin, flags);
	else if ( pID < 0 )        // if pID < 0, error from fork()
	{
		fputs("Failed to fork.\n", stderr);
		exit(1);
	}
	else                    // if pID > 0, then parent
	{
		do {
			temp_pid = wait( &childstatus ); // wait for child
		} while (temp_pid != pID);
	}
	return childstatus;
}

/* Description: Intializes array of character strings to null.
 * Params:
 *  	arr		array of cstrings.
 * 		size	size of array.
 * Returns: None.
 */
void init_array( char **arr, int size )
{
    int i;
    
    for (i = 0; i < size; ++i)
        *arr++ = NULL;
}

/* Description: Tokenizes a line into path and command flags.
 * Params:
 *  	line	string to be tokenized.
 *  	cmds	array of string to store the command flags.
 * Returns: None.
 */
void tokenize(char *line, char **cmds)
{
	char *token;
	char *delim = " :";	// handle spaces and ':'s (from getenv())
	char **p_cmds = cmds;
    int num;

	token = strtok( line, delim );
	
	if ( !token ) 	// if no tokens, just skip (could not find any tokens)
	{
		return;
	} else {
		num = strlen(token);
		*p_cmds = (char*)malloc( MAX_STRING_LENGTH );
		strncpy( *p_cmds, token, num );
		(*p_cmds)[num] = '\0'; 	// ensure trailing null
		p_cmds++;

		while ( (token = strtok(NULL, delim)) )
		{
			num = strlen(token);
			*p_cmds = (char*)malloc( MAX_STRING_LENGTH );
			strncpy( *p_cmds, token, num );
			(*p_cmds)[num] = '\0';
			p_cmds++;
		}
	}
}

/* Description: Free an array of strings.
 * Params:
 *  	str		string to be freed.
 *  	size	size of array to be freed.
 * Returns: 0 if successful.
 */
int free_array(char **str, int size)
{
    int i;

    for (i = 0; i < size; ++i)
    {
        free(str[i]);
        str[i] = NULL;
    }
    return 0;
}

/* Description: Clears input buffer to handle overloading of string characters.
 * Params: None.
 * Returns: None.
 */
void clear_buffer()
{
	int ch;
	while ( (ch = getchar()) != '\n' && ch != EOF );
	fputs("input size too large.\n", stderr);
}

/* Description: Gets input from the kapish shell.
 * Params:
 *  line	string to store kapish shell input.
 * Returns: 0 if successful, 1 otherwise.
 */
int get_input(char *line)
{
	fflush(stdout);
	if ( fgets(line, MAX_INPUT, stdin) != NULL ) // if capture input successful
	{
		char *newline = memchr(line, '\n', strlen(line)); // search & overwrite trailing '\n'
		
		if ( newline != NULL )
			*newline = '\0';
		if ( strlen(line) >= MAX_INPUT-1 )
			clear_buffer(); // clear buffer to handle overloading
	} else {
		puts( "" );	// handle ctrl+d
		exit(1);
	}
	return 0;
}

/* Description: kapish simulates a shell by forking a child process and
 *  	exec the program to run in that child process.
 * Params: None.
 * Returns: 0 if successful.
 */
int main ()
{
    setup_term();
    sigset( SIGINT, SIG_IGN );  							
    
    enum option { cd = 0, exit, quit } cmd;
    const char *pathVarConstant;
    pathVarConstant = getenv("PATH");
    char buffer[MAX_INPUT];
    char *line;
    
    // main program loop starts here
    while (1)
    {
        printf("%s", "? "); // kapish prompt

        char *commands[MAX_COMMANDS];
        char **p_commands;
      	init_array( commands, MAX_COMMANDS );
        char dir_path[MAX_INPUT] = "";
        char pathVar[PATH_LENGTH];
        char *paths[MAX_PATHS];
        char **p_paths;
        init_array( paths, MAX_PATHS);
        
		memset( buffer, 0, MAX_INPUT );
        line = buffer;
        get_input(line);
        
        if ( !(*line) ) // if line empty, skip (only enter key pressed)
            continue;
        
        tokenize( line, commands );
        
        if ( !(*commands) ) // if commands list is empty, skip (only spaces)
        	continue;

        // test for "built-in" commands
        if ( !strcmp(*commands, "cd") )
            cmd = cd;
        else if ( !strcmp(*commands, "exit") )
            cmd = exit;
        else if ( !strcmp(*commands, "quit") )
            cmd = quit;
        else
            cmd = EXECUTE_PATH;
        
        switch (cmd)
        {
            case (cd):
                p_commands = commands+1;    // concatenate strings (dir path)
                while (*p_commands)
                    strcat(dir_path, *(p_commands++));

                if ( !dir_path[0] )       // if empty or space, go to /home
                {
                    if ( chdir("/home") )
                        fputs("error changing dir to /home.\n", stderr);
                } else if ( !chdir(dir_path) ) {
                    // changed directory succeeded
                } else
                    fputs("invalid path.\n", stderr);
                break;
                
            case (exit):
                return 0;
                break;
                
            case (quit):
                return 0;
                break;

            default:
                strncpy(pathVar, pathVarConstant, PATH_LENGTH-1);
                pathVar[PATH_LENGTH-1] = '\0';
            
                if ( pathVar != NULL )
                    tokenize( pathVar, paths );
                
                p_paths = &paths[0];

                for (; *p_paths; ++p_paths)
                {
                    strcat(*p_paths, "/");
                    strcat(*p_paths, commands[0]);
                    if ( !access(*p_paths, F_OK) )  // if path exists, break from loop
                        break;
                }

                if ( !access(*p_paths, F_OK) ) // if path exists, then run it in a bash child
                {   
                    p_commands = commands;                    
                    fork_exec( *p_paths, p_commands );
                } else {
                    fputs("invalid path.\n", stderr);
                }
                break;
		}
		free_array(commands, MAX_COMMANDS);
		free_array(paths, MAX_PATHS);
    }
    return 0;
}


